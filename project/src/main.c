/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

int showmenu(SDL_Surface* screen,TTF_Font* font)
{
	Uint32 time;
	int x,y;
	const int NUMMENU=2;
	const char* labels[NUMMENU] = {"Continue", "Exit"};
	SDL_Surface* menus[NUMMENU];
	bool selected[NUMMENU]={0,0};
	SDL_Color color[2] = {{255,255,255},{255,0,0}};

	menus[0]=TTF_RenderText_Solid(font,labels[0],color[0]);
	menus[1]=TTF_RenderText_Solid(font,labels[1],color[0]);
	SDL_Rect pos[NUMMENU];
	pos[0].x=screen->clip_rect.w/2 - menus[0]->clip_rect.w/2;
	pos[0].y=screen->clip_rect.h/2 - menus[0]->clip_rect.h;
	pos[1].x=screen->clip_rect.w/2 - menus[0]->clip_rect.w/2;
	pos[1].y=screen->clip_rect.h/2 + menus[0]->clip_rect.h;

	SDL_FillRect(screen,&screen->clip_rect,SDL_MapRGB(screen->format,0x00,0x00,0x00));

	SDL_Event event;
	while(1)
	{
		time=SDL_GetTicks();
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
				  for(int i=0;i<NUMMENU;i++)
				    SDL_FreeSurface(menus[i]);
				  return 1;
				case SDL_MOUSEMOTION:
				  x=event.motion.x;
				  y=event.motion.y;
				  for(int i=0;i<NUMMENU;i++)
				  {
                    if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h)
					{
					  if(!selected[i])
					  {
						  selected[i]=1;
						  SDL_FreeSurface(menus[i]);
						  menus[i]=TTF_RenderText_Solid(font,labels[i],color[1]);
					  }  
					  }else{
						if(selected[i])
						{
							selected[i]=0;
							SDL_FreeSurface(menus[i]);
							menus[i]=TTF_RenderText_Solid(font,labels[i],color[0]);
						}
					  }		  
				}	
			  case SDL_MOUSEBUTTONDOWN:
			    x=event.button.x;
				y=event.button.y;
				for(int i=0;i<NUMMENU;i++)
				  if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h)
				   {  
					 for(int j=0;j<NUMMENU;j++)
					    SDL_FreeSurface(menus[j]);
					 return i;
				   }
                break;
				case SDL_MOUSEBUTTONDOWN:
			    x=event.button.x;
				y=event.button.y;
				for(int i=0;i<NUMMENU;i++)
				  if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h)
				   {  
					 for(int j=0;j<NUMMENU;j++)
					    SDL_FreeSurface(menus[j]);
					 return i;
				break;
			  case SDL_KEYDOWN:
			    if(event.key.keysym.sym==SDLK_ESCAPE)
				{
				    for(int i=0;i<NUMMENU;i++)
					    SDL_FreeSurface(menus[i]);
				  return 0;
				}		 	
		    }
		}
		for(int i=0;i<NUMMENU;i++)
		   SDL_BlitSurface(menus[i],NULL,screen,&pos[i]);
		SDL_Flip(screen);
		if(1000/30-(SDL_GetTicks()-time));
	}
}

               case SDLK_ESCAPE
static void capFrameRate(long *then, float *remainder);

int main(int argc, char *argv[])
{
	long then;
	float remainder;

	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	initSDL();

	atexit(cleanup);

	initGame();

	initStage();

	then = SDL_GetTicks();

	remainder = 0;
	int i=showmenu(screen,font);
	if(i==1)
	   running=false;

	while (1)
	{
		prepareScene();

		doInput();

		app.delegate.logic();

		app.delegate.draw();

		presentScene();

		capFrameRate(&then, &remainder);
	}

	return 0;
}

static void capFrameRate(long *then, float *remainder)
{
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)*remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1)
	{
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
